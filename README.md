# GAIA API - JavaScript

This repository hosts an API for accessing GAIA remote services.

## Installing

Use `npm install gaia-api-js` or `yarn add gaia-api-js`.

## Usign GAIA API

Before using this API, you have to initialize the service with the API key provided.

```javascript
const GAIA = require('gaia-api-js')
const service = new GAIA('550e8400-e29b-41d4-a716-446655440000')
```

All the returning values are dependant of the OpenAPI specification for GAIA. Please, check it out [here](https://app.swaggerhub.com/apis-docs/GAIA-Service/GAIA/1.0.0) first.

### Classification

You can classify any content using `GAIA.classification`. Here is an example.

```javascript
const result = await service.classification(
  'es', // Language code matching the standard ISO 639.
  'Real Madrid, en la cuerda floja', // Title of the content.
  '¡Cristiano la lía parda!', // Introduction of the content.
  'Dos golpes tan seguidos...', // Text of the content.
  'https://marca.com' // Source of the content.
)
```

Returning `result` will be an object with two parameters:
* `code` is a `string` with the result code (example: `200`)
* `data`is an `object` with the result data. Please check the OpenAPI specification of this data (`ClassificationResponseBody`) [here](https://app.swaggerhub.com/apis-docs/GAIA-Service/GAIA/1.0.0).

### Filtering

You can classify any content using `GAIA.filtering`. Here is an example.

```javascript
const result = await service.filtering(
  'es', // Language code matching the standard ISO 639.
  'Real Madrid, en la cuerda floja. Dos golpes tan seguidos...' // Text of the content.
)
```

Returning `result` will be an object with two parameters:
* `code` is a `string` with the result code (example: `200`)
* `data`is an `object` with the result data. Please check the OpenAPI specification of this data (`FilteringResponseBody`) [here](https://app.swaggerhub.com/apis-docs/GAIA-Service/GAIA/1.0.0).

## Error handling

Service will fire an `error` when an unexpected behaviour occurs. This error has the next structure:
* `code` is a `string` with the error code (example: `500`)
* `name` is a `string` to display the error name.
* `message` is a `string` to display the error message.
* `stack` is the error stack.

## Dependencies

* `axios` is used for asynchronous request.
