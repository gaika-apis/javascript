const axios = require('axios')

/**
 * URL where the service is.
 */
const URL = 'https://gaika-backend-server-develop.herokuapp.com'

/**
 * GAIA personalized error.
 * @param {error} error
 */
function GError(error) {
  let code
  let message
  if (error.response) {
    code = error.response.status
    message = error.response.data
  } else if (error.request) {
    code = 'NO-RESPONSE'
    message = 'No response received from the server; please, check server availability and your connection.'
  } else {
    code = 'UNKNOWN'
    message = 'Unknown error thrown, please check the stack.'
  }
  this.code = code.toString()
  this.name = `GAIA service threw an error code "${code}".`
  this.message = `${message}.`
  this.stack = error.stack
}
GError.prototype = Object.create(Error.prototype)
GError.prototype.constructor = GError

/**
 * Make a request to the GAIA service.
 * @param   {string} target Target to be requested.
 * @param   {string} language Language send to the target.
 * @param   {object} params Parameters send to the target.
 * @returns {{ code: string, data: object }} Response of the service.
 */
const request = async (target, language, params) => {
  const { key } = values
  try {
    const response = await axios({
      method: 'post',
      url: `${URL}/${target}`,
      data: {
        key,
        language,
        ...params,
      }
    })
    return {
      code: response.status,
      data: response.data
    }
  } catch (error) {
    throw new GError(error)
  }
}

const values = {}
/**
 * GAIA API: This package provides easy access to the GAIA service.
 * @class GAIA
 */
class GAIA {
  /**
   * Create a GAIA service instance.
   * @param {string} key API key provided by the service.
   * @constructs
   */
  constructor(key) {
    values.key = key
  }
  /**
   * Request a classification given the parameters.
   * @param   {string} language Language code matching the standard ISO 639.
   * @param   {string} title Title of the content.
   * @param   {string} introduction Introduction of the content.
   * @param   {string} text Text of the content.
   * @param   {string} source Source of the content (optional).
   * @returns {{ code: string, data: object }} Response of the service.
   * @async
   */
  async classification(language, text) {
    return await request('classification', language, {
      text,
    })
  }
  /**
   * Request a filtering given the parameters.
   * @param   {string} language Language code matching the standard ISO 639.
   * @param   {string} title Title of the content.
   * @param   {string} text Text of the content.
   * @returns {{ code: string, data: object }} Response of the service.
   * @async
   */
  async filtering(language, text) {
    return await request('filtering', language, {
      text,
    })
  }
}

module.exports = GAIA
